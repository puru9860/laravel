<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'brand' => $faker->company,
        'modelName' => $faker->unique()->word,
        'price' => $faker->randomFloat(2,100,10000),
        'user_id' => factory('App\User')
    ];
});
