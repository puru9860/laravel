@extends('layouts.app')

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if(count($products)>0)

                        <h3>Your ads</h3>
                            <table class="table">
                                <thead>
                                <tr>
                                    <th scope="col">S.N.</th>
                                    <th scope="col">Brand</th>
                                    <th scope="col">Model</th>
                                    <th scope="col">Price</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($products as $product)
                                    <tr>
                                        <th scope="row">
                                            {{$product->id}}
                                        </th>
                                        <td>
                                            {{$product->brand}}
                                        </td>
                                        <td>
                                            {{$product->modelName}}
                                        </td>
                                        <td>
                                            {{$product->price}}
                                        </td>
                                        <td>
                                            <div class="d-flex justify-content-end">
                                                <a href="\products\{{$product->id}}\edit" class="btn btn-primary mr-1">Edit</a>
                                                {!! Form::open(['action' => ['ProductsController@destroy',$product->id],'method' => 'DELETE','class'=>'d-inline']) !!}
                                                {{Form::submit('Delete',['class'=> "btn btn-danger "])}}

                                                {!!Form::close()!!}
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                    @endif

                </div>
            </div>
        </div>
    </div>

@endsection
