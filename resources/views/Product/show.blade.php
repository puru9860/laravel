@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                   <div class="title-wrap">
                       {{$product->brand.' '.$product->modelName}}
                   </div>
                    <div class="text-wrap">
                        <small class="">Posted on {{$product->created_at}} by {{$product->user->name}}</small>

                    </div>

                </div>

                <div class="card-body">
                    <div mt-5>Price: {{$product->price}}</div>

                </div>
            </div>
        </div>
    </div>



@endsection
