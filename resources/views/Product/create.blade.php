@extends('layouts.app')

@section('content')

        {!! Form::open(['action'=> 'ProductsController@store','method'=>'POST']) !!}
        <div class="row d-flex py-2">
            <div class="col-1 align-self-center">
                {{Form::label('brand', 'Brand', ['class'=>'m-0'])}}
            </div>
            <div class="col-6">
                {{Form::text('brand','', ['class'=>'form-control'])}}
            </div>
        </div>
        <div class="row d-flex py-2">
            <div class="col-1 align-self-center">
                {{Form::label('modelName', 'Model', ['class'=>'m-0'])}}
            </div>
            <div class="col-6">
                {{Form::text('modelName','', ['class'=>'form-control'])}}
            </div>
        </div>
        <div class="row d-flex py-2">
            <div class="col-1 align-self-center">
                {{Form::label('price', 'Price', ['class'=>'m-0'])}}
            </div>
            <div class="col-6">
                {{Form::text('price','', ['class'=>'form-control'])}}
            </div>
        </div>

        {!! Form::submit('Submit',['class'=> 'btn btn-success']) !!}

        {!! Form::close() !!}



@endsection
