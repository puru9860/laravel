<header>
    <div id="main-navbar" class="navbar navbar-expand-lg">

        <div class="left-content">
            <div class="logo">
                <a class="" href="\">
                    <img src="{{ asset('images\logo.png') }}">
                </a>
            </div>
            <div class="input-group search-bar">
                <input type="text" class="form-control" placeholder="Search">
                <div class="input-group-append">
                    <button class="btn btn-primary" type="button">
                        <i class="fa fa-search"></i>
                    </button>
                </div>
            </div>
        </div>
        <div class="right-content">
            <nav id="nav">
                <ul class="nav-links">
                    <li ><a href="\home" class="nav-link">Home</a></li>
                    <li><a href="{{route('products.index')}}" class="nav-item nav-link">Products</a></li>
                    <li ><a href="{{route('products.create')}}" class="nav-item nav-link">Create</a></li>

                <!-- Authentication Links -->
                @guest
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>
                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                    @endif
                @else
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                    @endguest
                    </ul>
            </nav>
        </div>

    </div>
</header>
