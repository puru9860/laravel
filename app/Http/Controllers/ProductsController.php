<?php

namespace App\Http\Controllers;

use App\Http\Resources\ProductResource;
use App\Product;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Request;

class ProductsController extends Controller
{

//    public function __construct()
//    {
//        $this->middleware('auth', ['except' => ['show', 'index']]);
//    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
//        return  ProductResource::collection($products);
        return view('product.index', [
            'products' => $products
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('product.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $this->validate($request, [
            'brand' => 'required',
            'modelName' => 'required',
            'price' => 'required'
        ]);
        $product = new Product($data);
        $product->user_id =1;
//        $product->user_id = auth()->user()->id;

//        $validators = Validator::($request->all(),[
//            'brand' => 'required',
//            'modelName' => 'required',
//            'price' => 'required'
//        ]);
        $product->save();
        return redirect('products');

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::find($id);
        return view('product.show', [
            'product' => $product
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);

        return view('product.edit', [
            'product' => $product
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $this->validate($request, [
            'brand' => 'required',
            'modelName' => 'required',
            'price' => 'required'
        ]);
        $product = Product::Find($id);
        $product->update($data);
        return redirect('home');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        $product->delete();
        return redirect('/home');
    }
}
